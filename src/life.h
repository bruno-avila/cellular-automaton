#ifndef LIFE_H
#define LIFE_H
#include <stdint.h>

enum cell_type{
    DEAD,
    ALIVE
};

void set_random_life(uint8_t *world, uint32_t grid_size);
void set_blank_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint32_t grid_size);
void draw_life(uint8_t *world, uint16_t width, uint32_t grid_size);
uint8_t cells_life(uint8_t *p, uint16_t width, uint16_t height, uint16_t i, uint16_t j);

void conways_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void high_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void seeds_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void without_death_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void day_and_night_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void h_trees_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void replicator_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void serviettes_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void iceballs_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void maze_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void maze_mice_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void mazectric_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void mazectric_mice_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void coral_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void bacteria_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void assimilation_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void long_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void holstein_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void amoeba_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void diamoeba_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void land_rush_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void bugs_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void geology_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void pedestrian_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void rings_n_slugs_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void vote_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
void twoxtwo_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size);
#endif