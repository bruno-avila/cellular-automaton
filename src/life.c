#include "life.h"
#include "raylib.h"
#include <stdint.h>
#include <string.h>

void set_random_life(uint8_t *world, uint32_t grid_size){
    for(uint8_t *p = world; p != world + grid_size; p++){
        if(GetRandomValue(0, 1) == ALIVE){
            *p = ALIVE;
        }else{
            *p = DEAD;
        }
    }
}

void set_blank_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint32_t grid_size){
    memset(world, DEAD, grid_size);
    memset(buffer, DEAD, grid_size);
}

void draw_life(uint8_t *world, uint16_t width, uint32_t grid_size){
    int i = 0; int j = 0;
    for(uint8_t *p = world; p!= world + grid_size; p++){
        if(*p == ALIVE){
            DrawPixel(i , j, WHITE);
        }else if(*p == DEAD){
            DrawPixel(i, j, BLACK);
        }
        i++;
        if(i == width){i = 0; j++;}
    }
}

uint8_t cells_life(uint8_t *p, uint16_t width, uint16_t height, uint16_t i, uint16_t j){
    uint8_t x[8];
    uint8_t y = 0;
    x[0] = *(p + ((i + width - 1) % width) + ((j + height- 1) % height) * width);
    x[1] = *(p + (i + ((j + height - 1) % height) * width));
    x[2] = *(p + ((i + width + 1) % width) + ((j + height - 1) % height) * width);
    x[3] = *(p + ((i + width - 1) % width) + j * width);
    x[4] = *(p + ((i + width + 1) % width) + j * width);
    x[5] = *(p + ((i + width - 1) % width) + ((j + height + 1) % height) * width);
    x[6] = *(p + (i + ((j + height + 1) % height) * width));
    x[7] = *(p + ((i + width + 1) % width) + ((j + height + 1) % height) * width);
    for(uint8_t z = 0; z < 8; z++){
        y += x[z];
    }
    return y;
}

void conways_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == ALIVE && (y < 2 || y > 3)){
                *(pg + pos) = DEAD;
            }else if(*(p + pos) == DEAD && y == 3){
                *(pg + pos) = ALIVE;
            }
        }
    }
}

void high_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == ALIVE && y != 2 && y != 3){
                *(pg + pos) = DEAD;
            }else if(*(p + pos) == DEAD && (y == 3 || y == 6)){
                *(pg + pos) = ALIVE;
            }
        }
    }
}

void seeds_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == ALIVE){
                *(pg + pos) = DEAD;
            }else if(*(p + pos) == DEAD && y == 2){
                *(pg + pos) = ALIVE;
            }
        }
    }
}

void without_death_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && y == 3){
                *(pg + pos) = ALIVE;
            }
        }
    }
}

void day_and_night_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y > 5)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y < 3 || y == 5)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void h_trees_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && y == 1){
                *(pg + pos) = ALIVE;
            }
        }
    }
}

void replicator_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(y % 2 != 0){
                *(pg + pos) = ALIVE;
            }else{
                *(pg + pos) = DEAD;
            }
        }
    }
}

void serviettes_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 2 || y == 3 || y == 4)){
                *(pg + pos) = ALIVE;
            }else{
                *(pg + pos) = DEAD;
            }
        }
    }
}

void iceballs_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 2 || y > 4)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y < 5){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void maze_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && y == 3){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y > 5 || y == 0)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void maze_mice_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 7)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y > 5 || y == 0)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void mazectric_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && y == 3){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y > 4 || y == 0)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void mazectric_mice_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 7)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y > 4 || y == 0)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void coral_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && y == 3){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y < 4){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void bacteria_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 4)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y < 4 || y > 6)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void assimilation_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y > 2 && y < 6)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y < 4 || y == 8)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void long_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y > 2 && y < 6)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y != 5){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void holstein_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y > 4)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y < 4 || y == 5)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void amoeba_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 5 || y == 7)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y == 0 || y == 2 || y == 4 || y == 6 || y == 7)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void diamoeba_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y > 4)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y < 5){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void land_rush_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 5)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y < 2 || y == 6)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void bugs_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || (y > 4 && y < 8))){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y < 5 && y != 1){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void geology_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 5 || y > 6)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y < 6 && y != 4 && y != 2){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void pedestrian_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 8)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y != 2 && y != 3){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void rings_n_slugs_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 5 || y == 6)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && (y == 0 || y == 2 || y == 3 || y == 7)){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void vote_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && y > 4){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y < 4){
                *(pg + pos) = DEAD;
            }
        }
    }
}

void twoxtwo_life(uint8_t *world, uint8_t *buffer, uint16_t width, uint16_t height, uint32_t grid_size){
    uint8_t y;
    uint8_t *p = world, *pg = buffer;
    uint32_t pos;
    for(uint16_t j = 0; j < height; j++){
        for(uint16_t i = 0; i < width; i++){
            y = 0;
            pos = i + j * width;
            y = cells_life(world, width, height, i, j);
            if(*(p + pos) == DEAD && (y == 3 || y == 6)){
                *(pg + pos) = ALIVE;
            }else if(*(p + pos) == ALIVE && y != 1  && y != 2 && y != 5){
                *(pg + pos) = DEAD;
            }
        }
    }
}