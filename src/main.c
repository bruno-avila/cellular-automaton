#include "raylib.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "life.h"

#define GRID_WIDTH 256
#define GRID_HEIGHT 256
#define SCREEN_WIDTH 720
#define SCREEN_HEIGHT 720

typedef struct Action{
    Vector2 mouse;
    uint8_t mouse_r;
    uint8_t mouse_l;
    uint8_t c;
    uint8_t m;
    uint8_t p;
    uint8_t r;
    uint8_t up;
    uint8_t down;
    uint8_t right;
    uint8_t left;
    uint8_t enter;
}Action;



typedef void (*set_rule)(uint8_t *, uint32_t);
typedef void (*run_rule)(uint8_t *, uint8_t *, uint16_t, uint16_t, uint32_t);
typedef void (*draw_rule)(uint8_t *, uint16_t, uint32_t);

struct Rules {
    char name[30];
    set_rule set;
    run_rule run;
    draw_rule draw;
}Rules;



struct Rules natures[] = {
    {"Conway's Game of Life", &set_random_life, &conways_life, &draw_life},
    {"HighLife", &set_random_life, &high_life, &draw_life},
    {"Seeds", &set_random_life, &seeds_life, &draw_life},
    {"Life Without Death", &set_random_life, &without_death_life, &draw_life},
    {"Day & Night", &set_random_life, &day_and_night_life, &draw_life},
    {"H-trees", &set_random_life, &h_trees_life, &draw_life},
    {"Replicator", &set_random_life, &replicator_life, &draw_life},
    {"Serviettes", &set_random_life, &serviettes_life, &draw_life},
    {"Iceballs", &set_random_life, &iceballs_life, &draw_life},
    {"Maze", &set_random_life, &maze_life, &draw_life},
    {"Maze with Mice", &set_random_life, &maze_mice_life, &draw_life},
    {"Mazectric", &set_random_life, &mazectric_life, &draw_life},
    {"Mazectric with Mice", &set_random_life, &mazectric_mice_life, &draw_life},
    {"Coral", &set_random_life, &coral_life, &draw_life},
    {"Bacteria", &set_random_life, &bacteria_life, &draw_life},
    {"Assimilation", &set_random_life, &assimilation_life, &draw_life},
    {"LongLife", &set_random_life, &long_life, &draw_life},
    {"Holstein", &set_random_life, &holstein_life, &draw_life},
    {"Amoeba", &set_random_life, &amoeba_life, &draw_life},
    {"Diamoeba", &set_random_life, &diamoeba_life, &draw_life},
    {"Land Rush", &set_random_life, &land_rush_life, &draw_life},
    {"Bugs", &set_random_life, &bugs_life, &draw_life},
    {"Geology", &set_random_life, &geology_life, &draw_life},
    {"Pedestrian Life", &set_random_life, &pedestrian_life, &draw_life},
    {"Rings 'n' Slugs", &set_random_life, &rings_n_slugs_life, &draw_life},
    {"Vote", &set_random_life, &vote_life, &draw_life},
    {"2x2", &set_random_life, &twoxtwo_life, &draw_life}
};


enum screen_state{
    MENU,
    LIFE
};

enum life_mode{
    CONWAY,
    HIGHLIFE
};

enum life_state{
    START,
    RUN,
    STOP
};

uint8_t screen = 0;
uint8_t life_state;
uint8_t life_mode = 0;
uint8_t menu_select = 0;
uint8_t life_options = sizeof(natures)/sizeof(natures[0]) - 1;// counts from 0
uint16_t fps = 15;

Action user_action = {0};

uint32_t grid_size = GRID_WIDTH * GRID_HEIGHT;

char menu_mode[20] = {"MODE <- ->"};
char menu_start[8] = {"START"};

unsigned char *world;
unsigned char *buffer;


void user_input(Camera2D camera);
void user_actions();
void menu();
void life_cycle();
void draw_menu();

int main(void){
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Cellular Automaton");
    Camera2D camera = {0};
    camera.target = (Vector2){0,0};
    camera.offset = (Vector2){SCREEN_WIDTH * 0.025f, SCREEN_HEIGHT * 0.025f};
    camera.zoom = 0.95f * SCREEN_HEIGHT / GRID_HEIGHT;
    SetTargetFPS(fps);
    world = calloc(grid_size, sizeof(*world));
    buffer = calloc(grid_size, sizeof(*buffer));

    while(!WindowShouldClose()){
        user_input(camera);
        switch(screen){
        case MENU:
            menu();
        break;
        case LIFE:
            life_cycle();
        break;
        }
        BeginDrawing();
            BeginMode2D(camera);
                switch(screen){
                case MENU:
                    draw_menu();
                break;
                case LIFE:
                    ClearBackground(GRAY);
                    natures[life_mode].draw(world, GRID_WIDTH, grid_size);
                    DrawPixel(user_action.mouse.x , user_action.mouse.y, GRAY);
                break;
                }
            EndMode2D();
        EndDrawing();
        memset(&user_action, 0, sizeof(user_action));
        SetTargetFPS(fps);
    }
    CloseWindow();
    return EXIT_SUCCESS;
}

void user_input(Camera2D camera){
    user_action.mouse = GetScreenToWorld2D(GetMousePosition(),camera);
    if(IsMouseButtonDown(MOUSE_RIGHT_BUTTON)){
        user_action.mouse_r = 1;
    }
    if(IsMouseButtonDown(MOUSE_LEFT_BUTTON)){
        user_action.mouse_l = 1;
    }
    if(IsKeyPressed(KEY_C)){
        user_action.c = 1;
    }
    if(IsKeyPressed(KEY_M)){
        user_action.m = 1;
    }
    if(IsKeyPressed(KEY_P)){
        user_action.p = 1;
    }
    if(IsKeyPressed(KEY_R)){
        user_action.r = 1;
    }
    if(IsKeyPressed(KEY_UP)){
        user_action.up = 1;
    }
    if(IsKeyPressed(KEY_DOWN)){
        user_action.down = 1;
    }
    if(IsKeyPressed(KEY_RIGHT)){
        user_action.right = 1;
    }
    if(IsKeyPressed(KEY_LEFT)){
        user_action.left = 1;
    }
    if(IsKeyPressed(KEY_ENTER)){
        user_action.enter = 1;
    }
}

void user_actions(){
    if(user_action.mouse.x < GRID_WIDTH  && user_action.mouse.x > 0 &&
        user_action.mouse.y < GRID_HEIGHT  && user_action.mouse.y > 0){
        if(user_action.mouse_l == 1){
            uint32_t x = user_action.mouse.x;
            uint32_t y = user_action.mouse.y;
            uint8_t *p = buffer + GRID_WIDTH * y + x;
            *p = ALIVE;
        }
        if(user_action.mouse_r == 1){
            uint32_t x = user_action.mouse.x;
            uint32_t y = user_action.mouse.y;
            uint8_t *p = buffer + GRID_WIDTH * y + x;
            *p = DEAD;
        }
    }
    if(screen == LIFE){
        if(user_action.c == 1){
            set_blank_life(world, buffer, GRID_WIDTH, grid_size);
        }
        if(user_action.m == 1){
            screen = MENU;
        }
        if(user_action.p == 1){
            if(life_state == STOP){
                life_state = RUN;
            }else{
                life_state = STOP;
            }
        }
        if(user_action.r == 1){
            set_random_life(world, grid_size);
        }
        if(user_action.right == 1){
            fps++;
        }
        if(user_action.left == 1 && fps > 1){
            fps--;
        }
    }
    
}

void life_cycle(){
    memcpy(buffer, world, grid_size);
    user_actions();
    switch(life_state){
    case START:
        natures[life_mode].set(world, grid_size);
        life_state = RUN;
    case RUN:
        natures[life_mode].run(world, buffer, GRID_WIDTH, GRID_HEIGHT, grid_size);
    break;
    case STOP:
    break;
    }
    memcpy(world, buffer, grid_size);
}

void menu(){
    if(user_action.down == 1){
        if(menu_select == 1){
            menu_select = 0;
        }else{
            menu_select++;
        }
    }
    if(user_action.up == 1){
        if(menu_select == 0){
            menu_select = 1;
        }else{
            menu_select--;
        }
    }
    if(menu_select == 0){
        if(user_action.right == 1){
            if(life_mode == life_options){
                life_mode = 0;
            }else{
                life_mode++;
            }
        }
        if(user_action.left == 1){
            if(life_mode == 0){
                life_mode = life_options;
            }else{
                life_mode--;
            }
        }
    }
    if(menu_select == 1 && user_action.enter == 1){
        screen = LIFE;
        life_state = START;
    }
}

void draw_menu(){
    float z = 0.95f * GRID_HEIGHT / 30;
    float y = (float) GRID_HEIGHT / 2 - z;
    ClearBackground(BLACK);
    Color x1 = GRAY;//mode
    Color x2 = GRAY;//start
    switch(menu_select){
    case 0:
        x1 = WHITE;
    break;
    case 1:
        x2 = WHITE;
    break;
    }
    DrawText(menu_mode, 0, y - z * 6, z, x1);
    DrawText(natures[life_mode].name, 0, y - z * 2, z, x1);
    DrawText(menu_start, 0, y + z * 2, z, x2);
}