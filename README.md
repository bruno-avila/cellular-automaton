# Cellular Automaton

Yet another implementation of Conway's Game of Life and other Life-like rules.

![Gif demonstration](./life.gif "demonstration")

## Controls
Use the Arrow Keys to select the desired rule and Enter to start.

Use Right and Left Mouse buttons to set cells to either black or white.

Right and Left Arrow Keys can be used to speed up and slow down the simulation.

C - Clears everything.

M - Returns to the Menu.

P - Pauses and resumes the simulation.

R - Sets the cells to random values.

## Build
Tested only on Linux.

## Dependencies
Requires glfw and raylib.

## Steps
Run make.
```
$ make
```

## License
MIT No Attribution (MIT-0)
